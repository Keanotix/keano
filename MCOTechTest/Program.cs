﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Xml;
using System.Xml.Linq;

namespace MCOTechTest
{

    class Program
    {
       
        static void Main(string[] args)
        {
            // Grab feed and save as anew Xml document
            var httpClient = new HttpClient();
            var result = httpClient.GetAsync("http://feeds.bbci.co.uk/news/uk/rss.xml").Result;
            var stream = result.Content.ReadAsStreamAsync().Result;
            var itemXml = XElement.Load(stream);
            var doc = new XmlDocument();


            if (itemXml != null && itemXml.ToString() != String.Empty) 
            {
                //Serialize Xml doc into JSON
                doc.LoadXml(itemXml.ToString());
                string jsonText = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.Indented);

                /*
                 * Create a new JSON object here to hold the new feed data (feedJSON) and an Json object to load previous files into (oldJSON)
                 * 
                 * Perform a loop that compares "items" object from each JSON object, if they match, move to next item.
                 * If there are items left, save these items to a new JSON object or remove the mathcing items from the feedJSON object.
                 * 
                 * Use this new object in place of jsonText when writing the file.
                 */

                //save resulting JSON to feed folder
                string filePath = Directory.GetCurrentDirectory();
                string timeStamp = DateTime.Now.ToString("yyyy-MM-dd-HH");
                Directory.CreateDirectory(filePath + @"\feed");
                File.WriteAllText(path: filePath + @"\feed\"+timeStamp+".json", contents: jsonText); //create timestamp string in filename
            }


            Console.WriteLine("File saved OK");
        }
    }
}
